# 1
with open('FILE.txt') as file:
    file = file.read().splitlines()
    N, M = map(int, file[0].split())
    lab = file[1:]
    for i in range(N):
        for j in range(M):
            print(lab[i][j], end='')
        print()

    space = 0
    int_lab = []
    for i in range(N):
        int_lab.append([])
        for j in range(M):
            if lab[i][j] != '*':
                int_lab[i].append(space)
            else:
                int_lab[i].append(1)

    int_lab2 = []
    for i in range(N):
        int_lab2.append([])
        for j in range(M):
            int_lab2[i].append(0)
# 2
from collections import deque

Q1 = deque()
Q2 = deque()
int_lab2[0][0] = 1
Q1.append((0, 0))
# 3

while Q1:
    a = Q1.pop()
    if a[1] < len(int_lab[0]) - 1 and int_lab2[a[0]][a[1] + 1] == space and int_lab[a[0]][a[1] + 1] == space:  # вправо
        int_lab2[a[0]][a[1] + 1] += 1 + int_lab2[a[0]][a[1]]
        Q2.append((a[0], a[1] + 1))
    if a[0] < len(int_lab) - 1 and int_lab2[a[0] + 1][a[1]] == space and int_lab[a[0] + 1][a[1]] == space:  # вниз
        int_lab2[a[0] + 1][a[1]] += 1 + int_lab2[a[0]][a[1]]
        Q2.append((a[0] + 1, a[1]))
    if a[1] > 0 and int_lab2[a[0]][a[1] - 1] == space and int_lab[a[0]][a[1] - 1] == space:  # влево
        int_lab2[a[0]][a[1] - 1] += 1 + int_lab2[a[0]][a[1]]
        Q2.append((a[0], a[1] - 1))
    if a[0] > 0 and int_lab2[a[0] - 1][a[1]] == space and int_lab[a[0] - 1][a[1]] == space:  # вверх
        int_lab2[a[0] - 1][a[1]] += 1 + int_lab2[a[0]][a[1]]
        Q2.append((a[0] - 1, a[1]))
    # 4
    Q1, Q2 = Q2, Q1
else:
    # 5
    end = int_lab2[len(int_lab2) - 1][len(int_lab2[0]) - 1]
    if end == space:
        print("Лабиринт не проходим")
    else:
        # вывод
        for i in range(N):
            for j in range(M):
                if int_lab2[i][j] != space:
                    print(f"{int_lab2[i][j]}\t", end='')
                else:
                    print('*\t', end='')
            print()
        print("Ответ:", end)
